# Evaluación full stack N1
Descripción del trabajo a presentar, para evaluación de capacidades de desarrollo web usando tecnologías HTML5, CSS3, JS y PHP. Se deberá realizar un formulario de contacto vía ajax, que envíe un email con ciertos campos y que valide los campos tanto en el browser como en el servidor. Se debe contemplar una correcta semántica en el HTML. Los puntos a evaluar figuran más adelante.

## Procedimiento:
    * Clonar este repositorio
    * Crear un repositorio nuevo (Github/Gitlab/etc...) en una cuenta personal, para este trabajo.
    * Desarrollar el sitio según los puntos a continuación.
    * Subir el trabajo finalizado en un servidor, si lo desea puede solicitar uno.
    * Actualizar repositorio del trabajo en el nuevo repositorio
    * Enviar link al repositorio y al demo.

1. Descripción de la estructura:
    * index.html contendrá solamente el HTML del sitio
    * app.js contendrá toda la lógica javascript se incluirá en este archivo
    * css.css si incluye estilos, use esta hoja css para los mismos
    * sendform.php procesará el envío del formulario, vía ajax

2. Requisitos:
    * El formulario se debe enviar vía AJAX(no usar submit común)
    * Al enviarse el formulario, el form dentro del html debe ocultarse, y seguidamente mostrar un mensaje de gracias por comunicarse.
    * Campos: Nombre, Email, Teléfono y Mensaje
    * Campos obligatorios: Email y Mensaje
    * Campos email de destino del form y asunto, configurables en sendform.php.
    * Indentado a 2 espacios (no tab)
    * Mostrar un mensaje de "Enviando..." cuando el usuario envíe el formulario.
    * Si el servidor devuelve un error, mostrar un mensaje "Error al enviar"

3. Requisitos Técnicos
    * Validar formulario HTML, tomando en cuenta formato de email y campos obligatorios
    * Validar todo el post en sendform.php, esto es chequear varialbes obligatorias, el formato y que vengan todas.
    * Usar mail() para enviar el email
    * Usar JSON para intercambiar datos vía ajax
    * jQuery 3.x
    * Plugin para validar formulario en browser (Ej.: validation-engine)
    * El maquetado debe Validar (https://validator.w3.org/)
    * El CSS debe validar (https://jigsaw.w3.org/css-validator/)

4. NO's
    * No usar js inline
    * No usar css inline
    * No usar php dentro del html
    * No cambiar la url al enviar el formulario.

## Puntos de interes de la evaluación:
1. Simplicidad del código js y php
2. Lectura del código js y php
3. Semántica del maquetado html
4. Seguridad del código php y js
5. Originalidad del código en general